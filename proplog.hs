{--
 Program 7


 ------------------------------------------------------------
 TEST CODE:
 a1. taut1 tauts
    True
 a2. taut1 tauts2
    True 
 a3. taut1 tauts3
    True
 ------------------------------------------------------------
 b1. sat1 sats                                              -
    True                                                    -
 b2. sat1 sats2                                             -
    True                                                    -
 b3. sat1 sats3                                             -
    True                                                    -
 ------------------------------------------------------------
 c1. unsat1 unsats                                          -
    True                                                    -
 c2. unsat1 unsats2                                         -
    True                                                    -
 c3. unsat1 unsats3                                         -
    True                                                    -
--}



-- definition of basic gates
data Prop = T | F |
    Not Prop |
    And Prop Prop |
    Or Prop Prop 
    deriving (Eq,Read,Show)


-- truth table of basic gates
tt (Not F) = T
tt (Not T) = F

tt (And F F) = F
tt (And F T) = F
tt (And T F) = F
tt (And T T) = T

tt (Or F F) = F
tt (Or F T) = T
tt (Or T F) = T
tt (Or T T) = T


--Giving the truth table (tt) of a derived gate
xor' F F = F
xor' F T = T
xor' T F = T
xor' T T = F

--Giving the truth table(tt) FOR NAND
--nand' F F = T
--nand' T F = T
--nand' F T = T
--nand' T T = F

--Giving the truth table(tt) FOR NOR 
--nor' F F = T 
--nor' T F = F 
--nor' F T = F 
--nor' T T = F

--building it from Not, And, Or of a derived gate
xor x y = eval (And (Or x y) (Not (And x y)))

eval T = T
eval F = F
eval (Not x) = tt (Not (eval x))
eval (And x y) = tt (And (eval x) (eval y))
eval (Or x y) = tt (Or (eval x) (eval y))
--------------------------------------------------------------------
-- PART ONE OF ASSIGNMENT:

--building NAND from Not, And, Or of a derived gate
nand' :: Prop -> Prop -> Prop
nand' x y = eval (Not (And x y))

--building NOR from Not, And, Or of a derived gate
nor' :: Prop -> Prop -> Prop
nor' x y = eval (Not (Or x y)) 
--------------------------------------------------------------------
-- PART TWO OF ASSIGNMENT:
---------------------------------------------------------------
-- Tautologies
tauts :: Prop -> Prop
tauts x = eval (xor x(nor' x x))

tauts2 :: Prop -> Prop
tauts2 x = eval (xor x(nand' x x))

tauts3 :: Prop -> Prop
tauts3 x = eval (nand' x(nor' x(nor' x x)))
-----------------------------------------------------------
--Satisfiable Formulas
sats :: Prop -> Prop
sats x = eval (xor x (nand' x x))

sats2 :: Prop -> Prop 
sats2 x = eval (impl x x)

sats3 :: Prop -> Prop
sats3 x = eval (xor x(impl x x))
-----------------------------------------------------------------
--Unsatisfiable Formulas
unsats :: Prop -> Prop
unsats x = eval (xor x x)

unsats2 :: Prop -> Prop 
unsats2 x = eval (nor' x(nand' x x))

unsats3 :: Prop -> Prop
unsats3 x = eval (nor' x(nor' x x))


-----------------------------------------------------------------

--tautTable1 f = [(x,f x ) | x<-[F,T]]

--if then else, condition, then, else

ite c t e = eval (Or (And c t) (And (Not c) e))

truthTable1 f = [(x,f x) | x<-[F,T]]

tt1 f = mapM_ print (truthTable1 f)

truthTable2 f = [((x,y),f x y) | x<-[F,T],y<-[F,T]]

tt2 f = mapM_ print (truthTable2 f)

truthTable3 f = [((x,y),f x y z) | x<-[F,T],y<-[F,T],z<-[F,T]]

tt3 f = mapM_ print (truthTable3 f)

truthTable4 f = [((x,y),f x y) | x<-[F,T],y<-[F,T]]
tt4 f = mapM_ print (truthTable4 f)

truthTable5 f = [((x,y),f x y) | x<-[F,T],y<-[F,T]]
tt5 f = mapM_ print (truthTable5 f)

or' x y = eval (Or x y) 
and' x y = eval (And x y) 
not' x = eval (Not x)

impl x y = eval (Or (Not x) y)

eq x y = eval (And (impl x y) (impl y x))

deMorgan1 x y = eq (Not (And x y)) (Or (Not x) (Not y))
deMorgan2 x y = eq (Not (Or x y)) (And (Not x) (Not y))


-- tautologies, satisfiable and unsatisfiable formulas

taut1 f = all (==T) [f x|x<-[F,T]]

taut2 f = all (==T) [f x y|x<-[F,T],y<-[F,T]]

sat1 f = any (==T) [f x|x<-[F,T]]

sat2 f = any (==T) [f x y|x<-[F,T],y<-[F,T]]

unsat1 f = not (sat1 f)
unsat2 f = not (sat2 f)

-- examples of tautologies: de Morgan1,2
-- examples of satisfiable formulas: xor, impl, ite

-- example of contradiction (unsatisfiable formulas): contr1
contr1 x = eval (And x (Not x))
-- :: Prop -> Prop

{- Assignment due April 21 at noon

1.) emulate nand, nor in terms of Not, Or, and And
2.) By using nand, nor, xor, impl, T, F, build:
	a. 3 tautologies
	b. 3 satisfiable formulas
	c. 3 unsatisfiable formulas
3.) draw on paper and scanf to PDF circuits corresponding to each of your formulas.
You can alse use a tool for drawing if you prefer.
-}
